package com.example.dao;

import java.util.List;

import com.example.model.Employee;
import com.example.model.RoleEmployee;

public interface EmpDao {
	List<Employee> getAll();

	List<RoleEmployee> getAllRole();

	Employee getEmpById(int id);

	RoleEmployee getEmpByRoleId(int id);

	void addEmp(Employee emp);

	void addEmpRole(RoleEmployee roleemp);

	void updateEmp(Employee emp, int id);

	void updateEmpRole(RoleEmployee roleemp, int roleid);
	
	void deleteEmpById(int id);
	
	void deleteEmpByRoleId(int roleid);
	
	int lastestInput();
	
	int lastestInputRole();
}
