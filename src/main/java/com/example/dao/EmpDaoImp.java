package com.example.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Employee;
import com.example.model.EmployeeMapper;
import com.example.model.RoleEmployee;
import com.example.model.RoleEmployeeMapper;

@Transactional

@Repository
public class EmpDaoImp implements EmpDao {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Employee> getAll() {
		String sql = "select * from employee order by employeeid asc";
		List<Employee> emp = jdbcTemplate.query(sql, new EmployeeMapper());
		return emp;
	}

	@Override
	public List<RoleEmployee> getAllRole() {
		String sql = "select * from employeerole order by employeeroleid asc";
		List<RoleEmployee> roleemp = jdbcTemplate.query(sql, new RoleEmployeeMapper());
		return roleemp;
	}

	@Override
	public Employee getEmpById(int id) {
		String sql2 = "select * from employee where employeeid = ?";

		Employee emp2 = jdbcTemplate.queryForObject(sql2, new Object[] { id }, new EmployeeMapper());
		return emp2;
	}

	@Override
	public RoleEmployee getEmpByRoleId(int id) {
		String sql2 = "select * from employeerole where employeeroleid = ?";

		RoleEmployee roleemp2 = jdbcTemplate.queryForObject(sql2, new Object[] { id }, new RoleEmployeeMapper());
		return roleemp2;
	}

	@Override
	public void addEmp(Employee emp) {
		String sql = "INSERT INTO employee (employeeroleid, employeename, phone, address) VALUES (?, ?, ?, ?) ";
		jdbcTemplate.update(sql,emp.getRoleId(), emp.getName(), emp.getPhone(), emp.getAddress());
	}

	@Override
	public void addEmpRole(RoleEmployee roleemp) {
		String sql = "INSERT INTO employeerole (employeerolename) VALUES (?) ";
		jdbcTemplate.update(sql, roleemp.getRoleName());
	}

	@Override
	public void updateEmp(Employee emp, int id) {
		String sql = "update employee set employeename=?, phone=? ,address=? where employeeid=?";
		jdbcTemplate.update(sql, emp.getName(), emp.getPhone(), emp.getAddress(), id);
	}

	@Override
	public void updateEmpRole(RoleEmployee roleemp, int roleid) {
		String sql = "update employeerole set employeerolename=? where employeeroleid=?";
		jdbcTemplate.update(sql, roleemp.getRoleName(), roleid);
	}

	@Override
	public int lastestInput() {
		String sql2 = "SELECT employeeid from employee order by employeeid desc limit 1";
		int id = jdbcTemplate.queryForObject(sql2, Integer.class);
		return id;
	}

	@Override
	public int lastestInputRole() {
		String sql2 = "SELECT employeeroleid from employeerole order by employeeroleid desc limit 1";
		int roleid = jdbcTemplate.queryForObject(sql2, Integer.class);
		return roleid;
	}

	@Override
	public void deleteEmpById(int id) {
		String sql = "delete from employee where employeeid=?";
		jdbcTemplate.update(sql, id);
		
	}

	@Override
	public void deleteEmpByRoleId(int roleid) {
		String sql = "delete from employeerole where employeeroleid=?";
		jdbcTemplate.update(sql, roleid);
		
	}

}
