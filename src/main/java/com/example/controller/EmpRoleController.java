package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.dao.EmpDao;
import com.example.model.RoleEmployee;

@Controller
@RequestMapping("roleemp")
public class EmpRoleController {
	@Autowired
	EmpDao empDao;
	
	@GetMapping("/{roleid}")
	public ResponseEntity<RoleEmployee> getById(@PathVariable("roleid") int roleid) {
		RoleEmployee roleemp = empDao.getEmpByRoleId(roleid);
		return new ResponseEntity<RoleEmployee>(roleemp, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<RoleEmployee>> getAll() {
		List<RoleEmployee> listRoleEmp = empDao.getAllRole();
		return new ResponseEntity<List<RoleEmployee>>(listRoleEmp, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<RoleEmployee> addEmpRole(@RequestBody RoleEmployee roleemp) {
		empDao.addEmpRole(roleemp);
		RoleEmployee roleemp2 = empDao.getEmpByRoleId(empDao.lastestInputRole());
		return new ResponseEntity<RoleEmployee>(roleemp2, HttpStatus.OK);
	}

	@PutMapping("/{roleid}")
	public ResponseEntity<RoleEmployee> updateEmpRole(@PathVariable("roleid") int roleid, @RequestBody RoleEmployee roleemp) {
		empDao.updateEmpRole(roleemp, roleid);
		RoleEmployee roleemp2 = empDao.getEmpByRoleId(roleid);
		return new ResponseEntity<RoleEmployee>(roleemp2, HttpStatus.OK);
	}
	
	@DeleteMapping("/{roleid}")
	public ResponseEntity<RoleEmployee> deleteEmpRole(@PathVariable("roleid") int roleid) {
		RoleEmployee roleemp2 = empDao.getEmpByRoleId(roleid);
		empDao.deleteEmpByRoleId(roleid);
		return new ResponseEntity<RoleEmployee>(roleemp2, HttpStatus.OK);
	}
}

