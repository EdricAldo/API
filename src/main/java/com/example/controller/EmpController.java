package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.dao.EmpDao;
import com.example.model.Employee;

@Controller
@RequestMapping("emp")
public class EmpController {
	@Autowired
	EmpDao empDao;
	
	@GetMapping("/{id}")
	public ResponseEntity<Employee> getById(@PathVariable("id") int id) {
		Employee emp = empDao.getEmpById(id);
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}

	@GetMapping("/")
	public ResponseEntity<List<Employee>> getAll() {
		List<Employee> listEmp = empDao.getAll();
		return new ResponseEntity<List<Employee>>(listEmp, HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Employee> addEmp(@RequestBody Employee emp) {
		empDao.addEmp(emp);
		Employee emp2 = empDao.getEmpById(empDao.lastestInput());
		return new ResponseEntity<Employee>(emp2, HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Employee> updateEmp(@PathVariable("id") int id, @RequestBody Employee emp) {
		empDao.updateEmp(emp, id);
		Employee emp2 = empDao.getEmpById(id);
		return new ResponseEntity<Employee>(emp2, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Employee> deleteEmp(@PathVariable("id") int id) {
		Employee emp2 = empDao.getEmpById(id);
		empDao.deleteEmpById(id);
		return new ResponseEntity<Employee>(emp2, HttpStatus.OK);
	}
}
