package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class EmployeeMapper implements RowMapper<Employee> {

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee empResult = new Employee();
		empResult.setId(rs.getInt("employeeid"));
		empResult.setRoleId(rs.getInt("employeeroleid"));
		empResult.setName(rs.getString("employeename"));
		empResult.setPhone(rs.getString("phone"));
		empResult.setAddress(rs.getString("address"));
		return empResult;
	}
}
