package com.example.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RoleEmployeeMapper implements RowMapper<RoleEmployee> {

	@Override
	public RoleEmployee mapRow(ResultSet rs, int rowNum) throws SQLException {
		RoleEmployee roleempResult = new RoleEmployee();
		roleempResult.setRoleId(rs.getInt("employeeroleid"));
		roleempResult.setRoleName(rs.getString("employeerolename"));
		return roleempResult;
	}

}
